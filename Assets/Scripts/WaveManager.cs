﻿using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public float TimeBetweenWaves = 30.0f;
    public float ChanceToSpawnPolice = 0.25f;
    public float ChanceToSpawnMilitia = 0.5f;
    public int CurrentWave;
    public float CurrentWaveTime;
    public float CurrentWavePercent;

    private EnemyUnitSpawner _unitSpawner;
    private Timer _timer;

    void Awake()
    {
        _unitSpawner = GameObject.Find("Pivot SpawnPoints").GetComponent<EnemyUnitSpawner>();
    }
	// Use this for initialization
	void Start () {
	    _timer = new Timer(TimeBetweenWaves,true,StartWave);
	    StartCoroutine(_timer.Start());
	}

    void Update()
    {
        CurrentWaveTime += Time.deltaTime;
        if (CurrentWaveTime >= TimeBetweenWaves)
        {
            CurrentWaveTime = 0;
        }
        CurrentWavePercent = CurrentWaveTime/TimeBetweenWaves;
        Messenger<float>.Broadcast("wave updated", CurrentWavePercent);
    }

    private void StartWave()
    {
        for (int i = 0; i < _unitSpawner.SpawnPoints.Count; i++)
        {
            var spawnChancePolice = Random.Range(0f, 1f);
            var spawnChanceMilitia = Random.Range(0f, 1f);

            if (spawnChanceMilitia >= spawnChancePolice)
            {
                if (spawnChanceMilitia <= ChanceToSpawnMilitia)
                {
                    //_unitSpawner.StartCoroutine(_unitSpawner.SpawnMilita(CalculateNumberToSpawn(), i));
                    Messenger<int>.Broadcast<bool>("remove money", 10, Success);
                }
            }
            else
            {
                var numberToSpawn = CalculateNumberToSpawn();
                if (spawnChancePolice <= ChanceToSpawnPolice && numberToSpawn != 0)
                {
                    Messenger<int, int, int>.Broadcast("arrego", 
                                                        CalculatePoliceFee(), 
                                                        numberToSpawn,
                                                        i,MessengerMode.DONT_REQUIRE_LISTENER);
                    
                    _unitSpawner.StartCoroutine(_unitSpawner.SpawnPolice(numberToSpawn, i));
                }
            }
        }
        CurrentWave++;
        Messenger<int>.Broadcast("wave started",CurrentWave);
        
    }

    void Success(bool b)
    {
        
    }

    private int CalculateNumberToSpawn()
    {
        var number = Mathf.Log(CurrentWave, 2);

        return (int)number;
    }

    int CalculatePoliceFee()
    {
        return CurrentWave*100;
    }
}

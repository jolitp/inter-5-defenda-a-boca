﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public InAudioNode FootSteps;
    public InAudioNode Cloths;
    public InAudioNode Breath;
    public InAudioNode Shot;

    private FogOfWarVisibility _fogOfWarVisibility;
    


    private List<Renderer> _renderers = new List<Renderer>();

    void Start()
    {
        
        _fogOfWarVisibility = GetComponent<FogOfWarVisibility>();
        for (int i = 0; i < transform.childCount; i++)
        {
            _renderers.Add(transform.GetChild(i).gameObject.GetComponentInChildren<Renderer>());
        }

    }

    


    public void PlayFootsteps()
    {
        if (_fogOfWarVisibility != null)
        {
            if (_fogOfWarVisibility.BeingObserved)
            {
                if (IsVisible())
                {
                    InAudio.Play(gameObject, FootSteps);
                }
            }
        }
        else
        {
            if (IsVisible())
            {
                InAudio.Play(gameObject, FootSteps);
            }
        }
    }

    bool IsVisible()
    {
        for (int i = 0; i < _renderers.Count; i++)
        {
            if (_renderers[i].isVisible)
            {
                return true;
            }
        }
        return false;
    }

    public void PlayBreath()
    {
        if (_fogOfWarVisibility.BeingObserved)
        {
            InAudio.Play(gameObject, Breath);
        }
    }

    public void PlayShot()
    {
        InAudio.Play(gameObject, Shot);
    }
}

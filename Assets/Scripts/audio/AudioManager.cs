﻿using UnityEngine;
using System.Collections;
using InAudioSystem.Runtime;

public class AudioManager : MonoBehaviour
{
    public InAudioNode Ambience;
    public InAudioNode StartWave;

    private GameManager _gameManager;

    private GameObject _audioSourceGo;
    
    public GameObject _musicStart;
    public GameObject _musicGame;
    public GameObject _musicEnd;

    void Start()
    {
        _audioSourceGo = GameObject.Find("AudioSource");
        _gameManager = GetComponent<GameManager>();

        StartCoroutine(PlayMusic());
        if (Application.loadedLevel == 1)
        {
            StartCoroutine(PlayAmbience());
        }
    }

    void OnEnable()
    {
        Messenger<int>.AddListener("wave started", PlayWaveSound);
    }

    void OnDisable()
    {
        Messenger<int>.RemoveListener("wave started", PlayWaveSound);
    }

    void PlayWaveSound(int i)
    {
        InAudio.Play(_audioSourceGo, StartWave);
    }


    //public void StopMusic()
    //{
    //    InAudio.StopAllAndMusic();
    //}



    IEnumerator PlayMusic()
    {
        while (true)
        {
            switch (_gameManager.CurrentScreen)
            {
                case GameScreenState.Start:
                    _musicStart.SetActive(true);
                    _musicGame.SetActive(false);
                    _musicEnd.SetActive(false);
                    break;
                case GameScreenState.Game:
                    _musicStart.SetActive(false);
                    _musicGame.SetActive(true);
                    _musicEnd.SetActive(false);
                    break;
                case GameScreenState.Credits:
                    _musicStart.SetActive(false);
                    _musicGame.SetActive(false);
                    _musicEnd.SetActive(true);
                    break;
            }

            yield return null;
        }
    }

    IEnumerator PlayAmbience()
    {
        while (true)
        {
            InAudio.Play(_audioSourceGo, Ambience);
            yield return new WaitForSeconds(23.0f);
        }
    }
}

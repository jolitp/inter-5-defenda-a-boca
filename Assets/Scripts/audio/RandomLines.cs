﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;


public class RandomLines : MonoBehaviour {

	const float TimeToRecycleLists = 15.0f;
	const float TimeToAlliesLines = 10.0f;
	const float TimeToEnemiesLines = 12.0f;


	public List<GameObject> policeList = new List<GameObject>();
	public List<GameObject> miliciaList = new List<GameObject>();
	public List<GameObject> vaporList = new List<GameObject>();
	public List<GameObject> falconList = new List<GameObject>();
	public List<GameObject> soldierList = new List<GameObject>();

	public GameObject policePool;
	public GameObject miliciaPool;
	public GameObject vaporPool;
	public GameObject falconPool;
	public GameObject soldierPool;



	void Start(){
		StartCoroutine (RecycleLists ());
		StartCoroutine (RandomizeLinesAllies());
		StartCoroutine (RandomizeLinesEnemies());
	}

	IEnumerator RecycleLists()
	{
		while (true) {

			ClearLists();
			PopulateLists();
			CheckVisibilityOfEnemies();
			SelectionCheck();

			yield return new WaitForSeconds(TimeToRecycleLists);
		}
	}

	void PopulateLists(){

		for (int i = 0; i < policePool.transform.childCount; i++) 
		{
			GameObject child = policePool.transform.GetChild(i).gameObject;
			policeList.Add(child);
		}

		for (int i = 0; i < miliciaPool.transform.childCount; i++) 
		{
			GameObject child = miliciaPool.transform.GetChild(i).gameObject;
			miliciaList.Add(child);
		}

		for (int i = 0; i < vaporPool.transform.childCount; i++) 
		{
			GameObject child = vaporPool.transform.GetChild(i).gameObject;
			vaporList.Add(child);
		}

		for (int i = 0; i < falconPool.transform.childCount; i++) 
		{
			GameObject child = falconPool.transform.GetChild(i).gameObject;
			falconList.Add(child);
		}

		for (int i = 0; i < soldierPool.transform.childCount; i++) 
		{
			GameObject child = soldierPool.transform.GetChild(i).gameObject;
			soldierList.Add(child);
		}
	}

	// clear list
	void ClearLists()
	{
		vaporList.Clear   ();
		soldierList.Clear ();
		falconList.Clear  ();
		policeList.Clear  ();
		miliciaList.Clear ();
	}

	bool PoliceVisible;
	bool MilitiaVisible;
	
	bool EnemyObserved(GameObject enemy)
	{
		var render = enemy.gameObject.GetComponentInChildren<Renderer> ();
		if (render != null) {
			if (render.isVisible) {
				return true;
			}
		}

		return false;
	}

	void CheckVisibilityOfEnemies()
	{
		for (int i = 0; i < policeList.Count; i++) {
			if(EnemyObserved(policeList[i]))
			{
				PoliceVisible = true;
			}

		}
		for (int i = 0; i < miliciaList.Count; i++) {
			if(EnemyObserved(miliciaList[i]))
				MilitiaVisible = true;
		}
	}

	public InAudioNode PoliceLines;
	public InAudioNode MilitiaLines;


	IEnumerator RandomizeLinesEnemies()
	{
		while (true) {
			
			int rnd = Random.Range (0, 2);
			
			switch (rnd) {
			case 0:
				if (PoliceVisible) {
					InAudio.Play (AudioSourceGO, PoliceLines);
					PoliceVisible = false;

				}
				break;
			case 1:
				if (MilitiaVisible) {
					InAudio.Play (AudioSourceGO, MilitiaLines);
					MilitiaVisible = false;
				}
				break;
			default:
				break;
			}
			
			float rnd2 = Random.Range(0.0f ,4.0f);
			
			yield return new WaitForSeconds(TimeToAlliesLines + rnd2);
		}
	}


	public InAudioNode VaporLines;
	public InAudioNode FalconLines;
	public InAudioNode SoldierLines;

	public GameObject AudioSourceGO;

	IEnumerator RandomizeLinesAllies()
	{
		while (true) {

			int rnd = Random.Range (0, 3);

			switch (rnd) {
			case 0:
				if (VaporSelected) {
					InAudio.Play (AudioSourceGO, VaporLines);
					Debug.Log("vapor sound");
				}
				break;
			case 1:
				if (FalconSelected) {
					InAudio.Play (AudioSourceGO, FalconLines);
					Debug.Log("falcon sound");
				}
				break;
			case 2:
				if (SoldierSelected) {
					InAudio.Play (AudioSourceGO, SoldierLines);
					Debug.Log("soldier sound");
				}
				break;
			default:
				break;
			}

			float rnd2 = Random.Range(0.0f ,4.0f);

			yield return new WaitForSeconds(TimeToAlliesLines + rnd2);
		}
	}


	bool VaporSelected;
	bool FalconSelected;
	bool SoldierSelected;
	
	void SelectionCheck()
	{
		FalconSelected = UnitConfirm (falconList);

		SoldierSelected = UnitConfirm (soldierList);

		VaporSelected = UnitConfirm (vaporList);
	}


	bool UnitConfirm(List<GameObject> list)
	{
		for (int i = 0; i < list.Count; i++) {
			if (UnitSelected(list[i])) {
				return true;
			}
		}
		return false;
	}

	bool UnitSelected(GameObject unit){
		var component = unit.GetComponentInChildren<Image> ();

	    if (component != null)
	    {
            if (component.IsActive())
            {
                return true;
            }
        }
        return false;
    }


}

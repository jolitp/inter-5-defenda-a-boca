﻿using System;
using UnityEngine;
using System.Collections;

public class AnimationControl : MonoBehaviour
{

    public float MoveSpeed
    {
        get { return _animator.GetFloat(_moveSpeedID);}
        set{_animator.SetFloat(_moveSpeedID,value);}
    }

    public float MoveDirection
    {
        get { return _animator.GetFloat(_moveDirectionID); }
        set { _animator.SetFloat(_moveDirectionID, value); }
    }
    public float AngularSpeed
    {
        get { return _animator.GetFloat(_angularSpeedID); }
        set { _animator.SetFloat(_angularSpeedID, value); }
    }

    public bool Attack
    {
        get { return _animator.GetBool(_attackingID); }
        set
        {
            _animator.SetBool(_attackingID, value);
            
        }
    }
    public bool BeingHit
    {
        get { return _animator.GetCurrentAnimatorStateInfo(1).IsName("GENHitFromFront"); }
        set { if (value) _animator.SetTrigger(_beingHitID); }
    }
    public bool Dead
    {
        get { return _animator.GetCurrentAnimatorStateInfo(1).IsName("NPCDyingA"); }
        set { if(value)_animator.SetTrigger(_isDeadID); }
    }

    private int _moveSpeedID;
    private int _moveDirectionID;
    private int _angularSpeedID;
    private int _attackingID;
    private int _beingHitID;
    private int _isDeadID;

    private Locomotion _locomotion;

    #region Cached variables

    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    private CombatControl _combatControl;
    private HealthControl _healthControl;

    #endregion
    void Awake()
    {
        _moveSpeedID = Animator.StringToHash("Speed");
        _moveDirectionID = Animator.StringToHash("Move Direction");
        _angularSpeedID = Animator.StringToHash("AngularSpeed");
        _attackingID = Animator.StringToHash("Attacking");
        _beingHitID = Animator.StringToHash("Being Hit");
        _isDeadID = Animator.StringToHash("IsDead");
    }
    void Start ()
	{
	    _animator = GetComponent<Animator>();
	    _navMeshAgent = GetComponentInParent<NavMeshAgent>();
	    _navMeshAgent.updateRotation = false;
        _locomotion = new Locomotion(_animator);
        _combatControl = GetComponentInParent<CombatControl>();
        _healthControl = GetComponentInParent<HealthControl>();
    }
	
	void Update ()
	{
        SetupAgentLocomotion();
	}

    void OnAnimatorMove()
    {
        if (!(Math.Abs(Time.deltaTime) > 0)) return;
        _navMeshAgent.velocity = _animator.deltaPosition / Time.deltaTime;
        transform.parent.rotation = _animator.rootRotation;
    }


    protected void SetupAgentLocomotion()
    {
        if (AgentDone())
        {
            _locomotion.Do(0, 0);
        }
        else
        {
            float speed = _navMeshAgent.desiredVelocity.magnitude;

            Vector3 velocity = Quaternion.Inverse(transform.rotation) * _navMeshAgent.desiredVelocity;

            float angle = Mathf.Atan2(velocity.x, velocity.z) * 180.0f / 3.14159f;

            _locomotion.Do(speed, angle);
        }
    }

    public bool AgentDone()
    {
        return !_navMeshAgent.pathPending && AgentStopping();
    }

    public bool AgentStopping()
    {
        return _navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance;
    }

    public void Shoot()
    {
        _combatControl.Shoot();
    }

    public void Die()
    {
        _healthControl.FadeMesh();
    }
}

﻿using UnityEngine;

public class RemoveFromPoolOnCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collision)
    {
        if(!collision.isTrigger) ObjectPool.Recycle(gameObject);
    }
}

﻿using UnityEngine;
using System.Collections;

public class FogOfWarSight : MonoBehaviour
{

    public GameObject SIghtRadius;
	public float radius = 10.0f;


	// Use this for initialization
	void Start ()
	{
	    var newScale = new Vector3(radius * 2, 0.001f, radius * 2);
	    SIghtRadius.transform.localScale = newScale;
	}

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy Unit Police" ||
            other.gameObject.tag == "Enemy Unit militia")
        {
            Messenger<GameObject>.Broadcast("Observed", other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy Unit Police" ||
            other.gameObject.tag == "Enemy Unit militia")
        {
            Messenger<GameObject>.Broadcast("not Observed", other.gameObject);
        }
    }
    
}

﻿using UnityEngine;
using System.Collections;

public class FogOfWarVisibility : MonoBehaviour
{
	public bool BeingObserved;
    private Renderer[] _renderer;
    private GameObject _healthBar;

    public GameObject Parent;
    public GameObject Mesh;
    
	void Start ()
	{
        FadeMesh(0);
	    _healthBar = transform.parent.gameObject.GetComponent<HealthControl>().HealthBarGameObject;
        _healthBar.SetActive(false);
    }
    

    void OnEnable()
    {
        Messenger<GameObject>.AddListener("Observed", Observed);
        Messenger<GameObject>.AddListener("not Observed", NotObserved);
    }

    void OnDisable()
    {
        Messenger<GameObject>.RemoveListener("Observed", Observed);
        Messenger<GameObject>.RemoveListener("not Observed", NotObserved);
    }

    void Observed(GameObject obj)
	{
        if (obj.GetInstanceID() != Parent.GetInstanceID()) return;
        _healthBar.SetActive(true);
        FadeMesh(1);
        BeingObserved = true;
	}

    void NotObserved(GameObject obj)
    {
        if (obj.GetInstanceID() != Parent.GetInstanceID()) return;
        _healthBar.SetActive(false);
        FadeMesh(0);
        BeingObserved = false;
    }

    public void FadeMesh(float alphaPercent)
    {
        iTween.FadeTo(Mesh, iTween.Hash(
                                  "alpha", alphaPercent,
                                  "includechildren", true,
                                  "time", .3f
            ));
    }
}

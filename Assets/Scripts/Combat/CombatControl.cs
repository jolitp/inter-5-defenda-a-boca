﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatControl : MonoBehaviour
{
    public float AttackRadius = 3;
    public float TimeBetweenShots = 1.0f;

    public LayerMask EnemyLayers;
    public GameObject BulletPrefab;

    private GameObject _gun;
    private Transform _bulletPoolParent;
    private bool _canShoot;
    private AnimationControl _animationControl;

    void Awake()
    {
        ObjectPool.CreatePool(BulletPrefab, 0);
        var pool = GameObject.Find("Pool");
        _bulletPoolParent = pool.transform.Find("Bullets Pool");

        _gun = transform.FindChild("Gun").gameObject;
    }

    void Start()
    {
        _animationControl = GetComponentInChildren<AnimationControl>();
    }
    void Update()
    {
        _animationControl.Attack = false;
        GameObject target = null;

        foreach (var col in Physics.OverlapSphere(transform.position, AttackRadius, EnemyLayers))
        {
            if (target == null)
            {
                target = col.gameObject;
            }
            else
            {
                var distToClosest = Vector3.Distance(transform.position, target.transform.position);
                var distToCollider = Vector3.Distance(transform.position, col.transform.position);
                if (distToCollider < distToClosest)
                {
                    target = col.gameObject;
                }
            }
        }

        if (target == null) return;
        if (_animationControl.AgentDone())
        {
            LookAtTarget(target.transform.position);
        }
        _animationControl.Attack = true;
    }

    private void LookAtTarget(Vector3 target)
    {
        iTween.LookTo(gameObject, target, 1.0f);
    }
    
    public void Shoot()
    {
        ObjectPool.Spawn(BulletPrefab, 
                        _bulletPoolParent, 
                        _gun.transform.position, 
                        transform.rotation);
    }

}

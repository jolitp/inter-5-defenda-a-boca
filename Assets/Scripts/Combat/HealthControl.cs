﻿using UnityEngine;
using UnityEngine.UI;

public class HealthControl : MonoBehaviour
{
    public float MaxHealth;
    public float CurrentHealth;
    public float CurrentHealthPercentage;
    public GameObject HealthBarGameObject;// { get; set; }
    public GameObject Mesh;

    private AnimationControl _animationControl;

	void Start ()
	{
	    //CurrentHealth = MaxHealth;
     //   CurrentHealthPercentage = CurrentHealth / MaxHealth;

	    _animationControl = GetComponentInChildren<AnimationControl>();
	}

    void OnEnable()
    {
        CurrentHealth = MaxHealth;
        CurrentHealthPercentage = CurrentHealth / MaxHealth;
        if (HealthBarGameObject != null)
        {
            Messenger<GameObject, float>.Broadcast("Update Fill", HealthBarGameObject, CurrentHealthPercentage, MessengerMode.DONT_REQUIRE_LISTENER);
            Messenger<GameObject, string>.Broadcast("Name Unit", HealthBarGameObject, gameObject.name, MessengerMode.DONT_REQUIRE_LISTENER);

        }
    }

    void OnTriggerEnter(Collider other)
    {
        _animationControl.BeingHit = false;
        if (other.gameObject.tag == "Bullet")
        {
            var damageTaken = other.gameObject.GetComponent<DamageInfo>().Damage;
            if (CurrentHealth >= 0)
            {
                CurrentHealth -= damageTaken;
                CurrentHealthPercentage = CurrentHealth/MaxHealth;
                Messenger<GameObject, float>.Broadcast("Update Fill", HealthBarGameObject, CurrentHealthPercentage);
                _animationControl.BeingHit = true;
            }
            else
            {
                _animationControl.Dead = true;
            }
        }
    }

     public void FadeMesh()
    {
        iTween.FadeTo(Mesh, iTween.Hash(
                                  "alpha", 0,
                                  "includechildren", true,
                                  "time", 1.0f,
                                  "oncomplete", "RecycleUnit",
                                  "oncompletetarget", gameObject
            ));
        
        HealthBarGameObject.Recycle();
    }

    public void RecycleUnit()
    {
        var newColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        var meshes = Mesh.GetComponentsInChildren<Renderer>();
        foreach (var m in meshes)
        {
            m.material.color = newColor;
        }

        ObjectPool.Recycle(gameObject);
        
    }
}

﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]
public class MoveForward : MonoBehaviour
{
    public float Speed = 100;

    private Rigidbody _rigidbody;
	// Use this for initialization
	void Awake ()
	{
	    _rigidbody = GetComponent<Rigidbody>();
	}

    void OnDisable()
    {
        _rigidbody.velocity = Vector3.zero;
    }

    void OnEnable()
    {
        _rigidbody.AddForce(transform.forward * Speed);
    }
}

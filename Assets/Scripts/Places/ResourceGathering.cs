﻿using UnityEngine;
using System.Collections;

public class ResourceGathering : MonoBehaviour
{
    public int MaxUnits = 3;
    public int CurrentUnits;
    public float TimeToGather = 3;

    private Timer _timer;
    private bool _beingGathered;

    void Start()
    {
        _timer = new Timer(TimeToGather,true,AddResource);
        StartCoroutine(_timer.Start());
    }

    void Awake()
    {
        CurrentUnits = 0;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player Unit Vapor")
        {
            OnVaporEntered();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player Unit Vapor")
        {
            OnVaporExited();
        }
    }

    void OnVaporEntered()
    {
        if (CurrentUnits < MaxUnits)
        {
            CurrentUnits++;
        }
        _beingGathered = true;
    }

    void OnVaporExited()
    {
        if (CurrentUnits > 0)
        {
            CurrentUnits--;
        }
        else
        {
            _beingGathered = false;
        }
        
    }

    void AddResource()
    {
        if (!_beingGathered) return;
        switch (CurrentUnits)
        {
            case 1: Messenger<int>.Broadcast("add money", 5);
                break;
            case 2: Messenger<int>.Broadcast("add money", 7);
                break;
            case 3: Messenger<int>.Broadcast("add money", 8);
                break;
        }
    }
}

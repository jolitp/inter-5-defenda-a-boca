﻿using UnityEngine;

[System.Serializable]

public class UnitSpawnPrefabs
{
    public GameObject UnitSoldier;

    public GameObject UnitVapor;

    public GameObject UnitFalcon;
    
}

public class PlayerUnitSpawner : MonoBehaviour
{

    // referencias para prefabs
    public UnitSpawnPrefabs Prefabs;

    public int SoldierCost = 30;
    public int VaporCost = 50;
    public int FalconCost = 100;

    private Transform _soldierPoolParent;
    private Transform _vaporPoolParent;
    private Transform _falconPoolParent;
    private bool _canSpawnVapor;
    private bool _canSpawnSoldier;
    private bool _canSpawnFalcon;
    private bool _canSpawn;

	// Use this for initialization
	void Awake () {
        ObjectPool.CreatePool(Prefabs.UnitSoldier, 0);
        ObjectPool.CreatePool(Prefabs.UnitVapor, 0);
        ObjectPool.CreatePool(Prefabs.UnitFalcon, 0);
        var pool = GameObject.Find("Pool");
        _soldierPoolParent = pool.transform.Find("Soldier Units Pool");
        _falconPoolParent = pool.transform.Find("Falcon Units Pool");
        _vaporPoolParent = pool.transform.Find("Vapor Units Pool");
	}

    void Start()
    {
        
    }

    public void SpawnVapor()
    {
        Messenger<int>.Broadcast<bool>("remove money", VaporCost, Success);
        if (!_canSpawn) return;
        var unit = ObjectPool.Spawn(Prefabs.UnitVapor, _vaporPoolParent);

        if (!unit.GetComponent<NavMeshAgent>().Warp(transform.position))
        {
            ObjectPool.Recycle(unit);
        }

    }
    public void SpawnSoldier()
    {

        Messenger<int>.Broadcast<bool>("remove money", SoldierCost, Success);
        if (!_canSpawn) return;
        var unit = ObjectPool.Spawn(Prefabs.UnitSoldier, _soldierPoolParent);

        if (!unit.GetComponent<NavMeshAgent>().Warp(transform.position))
        {
            ObjectPool.Recycle(unit);
        }
    }
    public void SpawnFalcon()
    {
        Messenger<int>.Broadcast<bool>("remove money", FalconCost, Success);
        if (!_canSpawn) return;
        var unit = ObjectPool.Spawn(Prefabs.UnitFalcon, _falconPoolParent);

        if (!unit.GetComponent<NavMeshAgent>().Warp(transform.position))
        {
            ObjectPool.Recycle(unit);
        }
    }

    private void Success(bool value)
    {
        _canSpawn = value;
    }
}

public static class Extensions
{
    public static void WarpInPlaceOrAroundRamdomically(this GameObject unit, Vector3 position)
    {
        var range = 10;
        bool spawned = false;
        NavMeshAgent navMeshAgent = unit.GetComponent<NavMeshAgent>();
        if (navMeshAgent.Warp(position))
        {
            return;
        }
        else
        {
            while (!spawned)
            {
                int rand = Random.Range(-range, range);
                for (int x = -range; x < range; x++)
                {
                    for (int z = -range; z < range; z++)
                    {
                        if (navMeshAgent.Warp(position + new Vector3(x, 0, z)))
                        {
                            return;
                        }
                    }
                }
            }
        }
    }
}

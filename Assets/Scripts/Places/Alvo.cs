﻿using UnityEngine;
using System.Collections;

public class Alvo : MonoBehaviour {

	public bool active = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider obj){
		if (obj.tag == "Vapor"){
			active = true;
		}
	}

	void OnTriggerExit(Collider obj){
		if (obj.tag == "Vapor"){
			active = false;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyResource : MonoBehaviour
{
    public int CurrentMoney;

    private Text _moneyText;

    void Awake()
    {
        _moneyText = GetComponent<Text>();
    }

    void Start()
    {
        _moneyText.text = CurrentMoney.ToString();
    }

    void OnEnable()
    {
        Messenger<int>.AddListener("add money", OnMoneyAdded);
        Messenger<int>.AddListener<bool>("remove money", OnMoneyRemoved);
    }

    void OnDisable()
    {
        Messenger<int>.RemoveListener("add money", OnMoneyAdded);
        Messenger<int>.AddListener<bool>("remove money", OnMoneyRemoved);
    }

    private void OnMoneyAdded(int amount)
    {
        CurrentMoney += amount;
        _moneyText.text = CurrentMoney.ToString();
    }

    private bool OnMoneyRemoved(int amount)
    {
        if (CurrentMoney >= amount)
        {
            CurrentMoney -= amount;
            _moneyText.text = CurrentMoney.ToString();
            return true;
        }
        return false;
    }
}

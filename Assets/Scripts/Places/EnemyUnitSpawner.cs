﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyUnitSpawner : MonoBehaviour
{
    public float TimeBetweenSpawns = 1.0f;

    public GameObject PolicePrefab;
    public GameObject MilitiaPrefab;
    public List<GameObject> SpawnPoints;

    private Transform _policePoolParent;
    private Transform _militiaPoolParent;

    void Awake()
    {
        ObjectPool.CreatePool(PolicePrefab, 0);
        ObjectPool.CreatePool(MilitiaPrefab, 0);
        var pool = GameObject.Find("Pool");
        _policePoolParent = pool.transform.Find("Police Units Pool");
        _militiaPoolParent = pool.transform.Find("Militia Units Pool");
    }

    void Start()
    {

    }

    void SpawnPolice(Vector3 spawnPoint)
    {
        var spawnedObject = ObjectPool.Spawn(PolicePrefab, _policePoolParent);
        if (!spawnedObject.GetComponent<NavMeshAgent>().Warp(spawnPoint))
        {
            float rand = Random.Range(-1, 1);
            spawnedObject.GetComponent<NavMeshAgent>().Warp(spawnPoint + new Vector3(rand, rand, rand));
        }
    }

    public IEnumerator SpawnPolice(int quantity, int index)
    {
        while (quantity > 0)
        {
            SpawnPolice(SpawnPoints[index].transform.position);

            quantity--;
            yield return new WaitForSeconds(TimeBetweenSpawns);
        }
    }

    void SpawnMilita(Vector3 spawnPoint)
    {
        var spawnedObject = ObjectPool.Spawn(MilitiaPrefab, _militiaPoolParent);
        if (!spawnedObject.GetComponent<NavMeshAgent>().Warp(spawnPoint))
        {
            float rand = Random.Range(-1, 1);
            spawnedObject.GetComponent<NavMeshAgent>().Warp(spawnPoint + new Vector3(rand, rand, rand));
        }
    }

    public IEnumerator SpawnMilita(int quantity, int index)
    {
        while (quantity > 0)
        {
            SpawnMilita(SpawnPoints[index].transform.position);

            quantity--;
            yield return new WaitForSeconds(TimeBetweenSpawns);
        }
    }
}

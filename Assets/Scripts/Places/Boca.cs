﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Boca : MonoBehaviour
{
    public enum Factions
    {
        None,
        Player,
        Enemy
    }
    // lista do estado de todas as bocas
    public static IList<Factions> CurrentDominium = new List<Factions>();
    public static int CurrentDominiumIndex; 
    public float TimeToCallForReinforcements = 10.0f;

    private Timer _spawnUnitsTimer;
    private Renderer _renderer;
    private Color _initialColor;
    private EnemyUnitSpawner _enemysSpawner;
    private WaveManager _waveManager;

    void Start()
    {
        _enemysSpawner = GameObject.Find("Pivot SpawnPoints").GetComponent<EnemyUnitSpawner>();
        _waveManager = GameObject.Find("Wave Manager").GetComponent<WaveManager>();

        _renderer = GetComponent<Renderer>();
        _initialColor = _renderer.material.color;
        _spawnUnitsTimer = new Timer(TimeToCallForReinforcements, true, SpawnReinfocements);

        AddToCurrentDominium();
    }

    private void SpawnReinfocements()
    {
        var numberOfSpawnPoints = _enemysSpawner.SpawnPoints.Count;
        StartCoroutine(_enemysSpawner.SpawnMilita(_waveManager.CurrentWave, Random.Range(0, numberOfSpawnPoints)));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy Unit militia")
        {
            _renderer.material.color = Color.red;
            StartCoroutine(_spawnUnitsTimer.Start());
            UpdateCurrentDominium(Factions.Enemy);
            Messenger.Broadcast("Enemy Enter Boca");
        }
        if (other.gameObject.tag == "Player Unit Vapor")
        {
            UpdateCurrentDominium(Factions.Player);
            Messenger.Broadcast("Player Enter Boca");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy Unit militia")
        {
            _renderer.material.color = _initialColor;
            _spawnUnitsTimer.Stop();
            UpdateCurrentDominium(Factions.None);
            Messenger.Broadcast("Enemy Exit Boca");
        }
        if (other.gameObject.tag == "Player Unit Vapor")
        {
            UpdateCurrentDominium(Factions.None);
            Messenger.Broadcast("Player Exit Boca");
        }
    }

    void UpdateCurrentDominium(Factions dominator)
    {
        CurrentDominium[CurrentDominiumIndex] = dominator;
    }

    void AddToCurrentDominium()
    {
        CurrentDominiumIndex = CurrentDominium.Count;
        CurrentDominium.Add(Factions.None);
    }
}

﻿using System;
using System.Collections;
using UnityEngine;

public enum GameScreenState
{
    Start,
    Instructions,
    Credits,
    Game
}
public class GameManager : MonoBehaviour
{
    
    public GameObject InstructionsCanvas;

    private GameScreenState _currentScreen;

    public GameScreenState CurrentScreen {
        get { return _currentScreen; }
        set
        {
            _currentScreen = value;
        }
    }

    public void ChangeToStartScreen()
    {
        _currentScreen = GameScreenState.Start;
        InAudio.StopAllAndMusic();
        Application.LoadLevel(0);
    }

    public void EndInstructions()
    {
        InstructionsCanvas.SetActive(false);
    }

    public void ChangeToCreditsScreen()
    {
        _currentScreen = GameScreenState.Credits;
        InAudio.StopAllAndMusic();
        Application.LoadLevel(2);

        GamePaused = false;
    }
    public void ChangeToGameScreen()
    {
        _currentScreen = GameScreenState.Game;
        InAudio.StopAllAndMusic();
        Application.LoadLevel(1);

        GamePaused = false;
    }

    void EnableCanvas(GameObject canvas)
    {
        canvas.SetActive(true);
        canvas.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void Terminate()
    {
        Application.Quit();
    }
    
    public bool GamePaused;


    private AudioManager _audioManager;
	// Use this for initialization
	void Start ()
	{
	    _audioManager = GetComponent<AudioManager>();
        if (Application.loadedLevel == 0) { }
	    CurrentScreen = GameScreenState.Start;
        if (Application.loadedLevel == 1)
            CurrentScreen = GameScreenState.Instructions;
        if (Application.loadedLevel == 2)
            CurrentScreen = GameScreenState.Credits;

    }

    // Update is called once per frame
    void Update ()
	{
	    Time.timeScale = GamePaused ? 0 : 1;


        //switch (CurrentScreen)
        //{
        //    case GameScreenState.Start:
        //        ChangeToStartScreen();
        //        break;
        //    case GameScreenState.Instructions:

        //        break;
        //    case GameScreenState.Credits:
        //        ChangeToCreditsScreen();
        //        break;
        //    case GameScreenState.Game:
        //        ChangeToGameScreen();
        //        break;
        //}
    }

    void OnEnable()
    {
        Messenger.AddListener("game paused", PauseGame);
        Messenger.AddListener("game unpaused", UnpouseGame);
        Messenger.AddListener("End Game", ChangeToCreditsScreen);
    }

    void OnDisable()
    {
        Messenger.RemoveListener("game paused", PauseGame);
        Messenger.RemoveListener("game unpaused", UnpouseGame);
        Messenger.RemoveListener("End Game", ChangeToCreditsScreen);
    }

    void PauseGame()
    {
        GamePaused = true;
    }

    void UnpouseGame()
    {
        GamePaused = false;
    }

    public void PauseToggle()
    {
        GamePaused = !GamePaused;
    }
    
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class contagem : MonoBehaviour {

	public int total;
	public float tempo;
	public float ciclo;

	public int atual;

	void Start () {
		tempo = 0;
	}

	void Update () {
		tempo += Time.deltaTime;
		if (tempo >= ciclo){
			countUp();
//			Debug.Log("upd");
			tempo = 0;
		}
	}

	void countUp (){
		if (atual < total){
			atual += 1;
//			Debug.Log("counted");
			gameObject.GetComponentInChildren<Text>().text = atual.ToString();
		}

	}

	void zerar(){
		atual = 0;
	}

}

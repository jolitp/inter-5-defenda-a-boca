﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NameDisplay : MonoBehaviour
{

    private Text _text;

    void Start()
    {
        _text = GetComponent<Text>();
    }

    void OnEnable()
    {
        Messenger<GameObject, string>.AddListener("Name Unit", UpdateName);
    }

    void OnDisable()
    {
        Messenger<GameObject, string>.RemoveListener("Name Unit", UpdateName);
    }

    void UpdateName(GameObject g, string n)
    {
        if (!g.Equals(gameObject)) return;
        Debug.Log("name changed");
        if (n.Contains("Police"))
        {
            _text.text = "Polícia";
        }
        if (n.Contains("Militia"))
        {
            _text.text = "Milícia";
        }
        if (n.Contains("Vapor"))
        {
            _text.text = "Vapor";
        }
        if (n.Contains("Falcon"))
        {
            _text.text = "Falcão";
        }
        if (n.Contains("Soldier"))
        {
            _text.text = "Traficante";
        }
    }
}

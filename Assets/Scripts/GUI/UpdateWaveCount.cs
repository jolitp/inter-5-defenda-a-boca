﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateWaveCount : MonoBehaviour
{

    private Text _text;

	// Use this for initialization
	void Start ()
	{
	    _text = GetComponent<Text>();
	}

    void OnEnable()
    {
        Messenger<int>.AddListener("wave started", OnWaveStarted);
    }

    void OnDisable()
    {
        Messenger<int>.RemoveListener("wave started", OnWaveStarted);
    }

    void OnWaveStarted(int waveNumber)
    {
        _text.text = "Onda Atual: " + waveNumber;
    }
}

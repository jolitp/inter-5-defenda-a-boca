﻿using UnityEngine;
public class ProjectOnScreenSpace : MonoBehaviour {

    public Canvas TargetCanvas;
    public GameObject Prefab;

    private RectTransform _projectedGameObject;
    private HealthControl _healthControl;
    private GameObject _healthBarPool;
    private Vector3 _initialScale;


    void Awake()
    {
        _initialScale = Prefab.transform.localScale;
    }
	void Start ()
	{
        TargetCanvas = GameObject.Find("Camera Canvas").GetComponent<Canvas>();
	    _healthControl = GetComponentInParent<HealthControl>();
	    _healthBarPool = GameObject.Find("HealthBar Pool").gameObject;
       // var projectedObject = Instantiate(Prefab);
        ObjectPool.CreatePool(Prefab,0);
	    var projectedObject = ObjectPool.Spawn(Prefab);
	    _projectedGameObject = projectedObject.GetComponent<RectTransform>();
        _projectedGameObject.transform.SetParent(_healthBarPool.transform, false);
	    _healthControl.HealthBarGameObject = projectedObject;
	    projectedObject.transform.localScale = _initialScale;
	}
	
	// Update is called once per frame
	void Update ()
    {
        var worldPos = transform.position;
        
        if(_projectedGameObject != null)
        _projectedGameObject.anchoredPosition = TargetCanvas.WorldToCanvas(worldPos);

    }
    
}

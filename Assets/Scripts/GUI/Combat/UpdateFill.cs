﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateFill : MonoBehaviour
{
    private GameObject _parentGameObject;
    private Image _image;
    void OnEnable()
    {
        _parentGameObject = transform.parent.gameObject;
        _image = GetComponent<Image>();
        Messenger<GameObject,float>.AddListener("Update Fill", OnFillChanged);
        OnFillChanged(_parentGameObject, 1);
    }

    void OnDisabled()
    {
        Messenger<GameObject,float>.RemoveListener("Update Fill", OnFillChanged);
    }

    void OnFillChanged(GameObject target,float newAmount)
    {
        if (_parentGameObject == target)
        {

            _image.fillAmount = newAmount;
        }
    }
    
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Story : MonoBehaviour {

	private string[] name = {
		"João",
		"José",
		"Zé",
		"Carlinhos",
		"Itatiba",
		"Amaral",
		"Arlindo",
		"Paulo",
		"Jaime",
		"Ademar",
		"Francisco",
		"Edu"
	};

	private string[] surname = {
		"Motosserra",
		"Mata Leão",
		"Orelha",
		"Feijão",
		"Piolho",
		"Canela",
		"Baixinho",
		"Nego",
		"Porco",
		"Chulé",
		"Pêra",
		"Casca Grossa",
	};

	string[] from = {
		"nascido na própria Cidade de Deus,",
		"nascido em Pinheiral,",
		"nascido em Barra Mansa,",
		"nascido em Parati,",
		"nascido em Natividade,",
		"nascido em Vassouras,",
		"nascido em Volta Redonda,",
		"nascido em Saquarema,",
		"nascido em Porto Real,",
		"nascido em Resende,",
		"nascido em Rio das Ostras,",
		"nascido em Mendes,",
		"nascido em Marica,",
		"nascido em Petrópolis,",
		"nascido em Cordeiro,",
		"nascido em Cantagalo,",
		"nascido em Miracema,",
		"nascido em Queimados,"
	};



	string[] trivia = {
		"Conhecido por empalar qualquer pessoa que se opusesse a seu ideal.",
		"Conhecido por sua sede de sangue.",
		"Conhecido por lista incontável de assassinatos.",
		"Conhecido por sua forma autoritária de tratar a população.",
		"Conhecido por usar apenas armas brancas.",
		"Conhecido por estrangular quem estivesse em seu caminho.",
		"Conhecido por ser o rei das mulheres.",
		"Conhecido por ser extremamente violento.",
		"Conhecido pela sua fama de negociante.",
		"Conhecido pelo seu grande carisma."	
	};


	string[] motivation = {
		"Seu objetivo era chegar até o topo do poder, dar tudo que o estado não podia dar.",
		"Sempre teve raiva da polícia, dizia que aquela vida não precisava ser submissa a eles.",
		"Gostava de matar e fazer o serviço sujo do crime.",
		"Por se um ótimo negociador, sempre conseguiu fazer dinheiro rápido.",
		"Ficou obcecado pelo poder e pelo controle, queria dominar todas as regiões.",
		"Teve uma infância difícil, e só deixaria de passar fome trabalhando para o tráfico.",
		"Depois que a família morreu pra uma facção rival, ele dedicou a vida por essa vingança.",
		"Queria levantar a favela e organizar o crime quando entrou no tráfico.",
		"Nunca teve nada a vida inteira, e no tráfico achou a chance de conquistar algo.",
		"Por ser ambicioso, queria ganhar todo o dinheiro que fosse possível."
	};

	string[] assension = {
		"Como era o braço direito do chefe, após sua morte tomou tudo que era dele.",
		"Se aliou com os criminosos mais influentes do tráfico na prisão.",
		"Quando a maioria dos chefes foram presos, não restava mais ninguém para assumir.",
		"A boca prosperava na mão dele, a liderança naturalmente foi pra ele.",
		"Depois que matou toda a concorrência, só restava ele para comandar a região.",
		"Depois de tanto tempo no ramo do tráfico, ele seguiu e fez o próprio negócio.",
		"Quando eliminou a facção rival, ele mesmo tomou conta da região deles.",
		"Depois que uniu todas as facções pequenas, começou a comanda-las.",
		"Subia cada vez mais de posição no tráfico, até um dia que virou o comando.",
		"Como era bom com a matemática, entrou rápido na gerência do crime."
	};

	string currentBio;

	void Start()
	{
		var nameRnd = Random.Range (0, name.Length);
		var surnameRnd = Random.Range (0, surname.Length);
		var fromRnd = Random.Range (0, from.Length);
		var triviaRnd = Random.Range (0, trivia.Length);
		var motivationRnd = Random.Range (0, motivation.Length);
		var assensionRnd = Random.Range (0, assension.Length);

		string nameVar = this.name [nameRnd];
		string surnameVar = this.surname [surnameRnd];
		string fromVar = this.from [fromRnd];
		string triviaVar = this.trivia [triviaRnd];
		string motivationVar = this.motivation [motivationRnd];
		string assensionVar = this.assension [assensionRnd];

		currentBio = nameVar + " " +
					 surnameVar + " " + 
				     fromVar + " " + 
					 triviaVar + " " + 
				     motivationVar + " " + 
				     assensionVar;
	    var textField = GetComponent<Text>();
	    textField.text = currentBio;
	}





















}

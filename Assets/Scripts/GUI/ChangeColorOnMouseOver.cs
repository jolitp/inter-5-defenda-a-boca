﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeColorOnMouseOver : MonoBehaviour
{
    private Image _image;

	// Use this for initialization
	void Start ()
	{
	    _image = GetComponent<Image>();
	}

    public void ChangeColorToRed()
    {
        _image.color = Color.red;
    }

    public void ChangeColorToWhite()
    {
        _image.color = Color.white;
    }
}

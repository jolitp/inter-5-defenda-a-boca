﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BocasDysplay : MonoBehaviour
{
    public int PlayerBocasQuantity;
    public int EnemyBocasQuantity;

    public Text PlayerBocasQuantityText;
    public Text EnemyBocasQuantityText;

    void OnEnable()
    {
        Messenger.AddListener("Enemy Enter Boca", AddEnemy);
        Messenger.AddListener("Enemy Exit Boca", RemoveEnemy);
        Messenger.AddListener("Player Enter Boca", AddPlayer);
        Messenger.AddListener("Player Exit Boca", RemovePlayer);
    }

    void OnDisable()
    {
        Messenger.RemoveListener("Enemy Enter Boca", AddEnemy);
        Messenger.RemoveListener("Enemy Exit Boca", RemoveEnemy);
        Messenger.RemoveListener("Player Enter Boca", AddPlayer);
        Messenger.RemoveListener("Player Exit Boca", RemovePlayer);
    }

    void AddEnemy()
    {
        EnemyBocasQuantity++;
        RefreshEnemyText();
    }
    void RemoveEnemy()
    {
        EnemyBocasQuantity--;
        RefreshEnemyText();
    }
    void RefreshEnemyText()
    {
       EnemyBocasQuantityText.text = EnemyBocasQuantity.ToString();
    }


    void AddPlayer()
    {
        PlayerBocasQuantity++;
        RefreshPlayerText();
    }
    void RemovePlayer()
    {
        PlayerBocasQuantity--;
        RefreshPlayerText();
    }
    void RefreshPlayerText()
    {
        PlayerBocasQuantityText.text = PlayerBocasQuantity.ToString();
    }

}

﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour
{

    private CanvasGroup _canvasGroup;

	public float passo;
	public bool up = false; //variavel que controla o aumento do canal alpha do canvasGroup
	public bool down = false; //variavel que controla a diminuicao do canal aplha do canvasGroup

    void Start()
    {
        _canvasGroup = gameObject.GetComponent<CanvasGroup>();
    }

	void Update () {
		if (up){
			FadeInInternal();
		}

		if (down){
			FadeOutInternal();
		}
        
	}

    public void FadeIn()
    {
            StartCoroutine(FadeInRoutine());
        up = true;
    }
    public void FadeOut()
    {
            StartCoroutine(FadeOutRoutine());
        down = true;
    }

    IEnumerator FadeInRoutine()
    {
        while (up)
        {
            FadeInInternal();

            yield return null;
        }
    }

     IEnumerator FadeOutRoutine()
    {
        while (down)
        {
            FadeOutInternal();

            yield return null;
        }
    }

    void FadeOutInternal(){ //funcao de diminuicao gradativa do alpha ate 0

		if (_canvasGroup.alpha > 0){
			if ((_canvasGroup.alpha - passo * Time.deltaTime) < 0){
                _canvasGroup.alpha = 0;
			}
			else
			{
                _canvasGroup.alpha -= passo * Time.deltaTime;
			}
		}
		else
		{
		    _canvasGroup.blocksRaycasts = false;
		    _canvasGroup.interactable = false;
            down = false;
		}

	}

	void  FadeInInternal(){ // funcao de aumento gradativo do alpha
		
		if (_canvasGroup.alpha < 1){
			if ((_canvasGroup.alpha + passo * Time.deltaTime) > 1 ){
                _canvasGroup.alpha = 1;
			}
			else
			{
                _canvasGroup.alpha += passo * Time.deltaTime;
			}
		}
		else
		{
		    _canvasGroup.blocksRaycasts = true;
            _canvasGroup.interactable = true;
            up = false;
		}
		
	}
}

﻿using System;
using UnityEngine;
using System.Collections;
using System.Globalization;
using UnityEngine.UI;

public class CountTime : MonoBehaviour
{
    private Text _text;

	// Use this for initialization
	void Start ()
	{
	    _text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var rawTime = Time.time;
	    //var min = rawTime/60;
	    //var sec = rawTime%60;
	    //_text.text = min + " : " + sec;
	    _text.text = rawTime.FloatToTime("#0:00");
	}
}

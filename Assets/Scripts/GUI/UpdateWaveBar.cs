﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class UpdateWaveBar : MonoBehaviour
{

    private Image _image;

    void Start()
    {
        _image = GetComponent<Image>();
    }

    void OnEnable()
    {
        Messenger<float>.AddListener("wave updated", OnWaveUpdate);
    }

    void OnDisable()
    {
        Messenger<float>.RemoveListener("wave updated", OnWaveUpdate);
    }

    void OnWaveUpdate(float percent)
    {
        _image.fillAmount = percent;
    }
}

﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Contains("Militia") ||
            other.gameObject.name.Contains("Police")
            )
        {
            Messenger.Broadcast("End Game");
        }
    }

    public void ForceEnd()
    {
        Messenger.Broadcast("End Game");
    }
}

﻿using UnityEngine;
using System.Collections;

public class MilitiaBehavior : EnemyBehavior
{
    public float TimeToCallForReinforcements = 10.0f;
    public GameObject MilitiaPrefab;

    public LayerMask BocaLayerMask;
    private Color _targetBocaInitialColor;
    private Timer _spawnUnitsTimer;
    private GameObject _currentBoca;

    void Awake()
    {
        Initialize();
    }

    void Start()
    {
        ChangeState(CurrentState);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, SightRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackRadius);

    }
    protected override void ChangeState(AvaliableStates active)
    {
        StopAllCoroutines();

        CurrentState = active;

        switch (CurrentState)
        {
            case AvaliableStates.Attack:
                StartCoroutine(AI_Attack());
                return;
            case AvaliableStates.Chase:
                StartCoroutine(AI_Chase());
                return;
            case AvaliableStates.Patrol:
                StartCoroutine(AI_Patrol());
                return;
            case AvaliableStates.HoldingBoca:
                StartCoroutine(AI_HoldBoca());
                return;
        }
    }
    protected override IEnumerator AI_Patrol()
    {
        while (true)
        {
            if (EnemySeen(out ChaseTarget))
            {
                ChangeState(AvaliableStates.Chase);
            }
            else
            {
                if (BocaInSight(out _currentBoca))
                {
                    ChangeState(AvaliableStates.HoldingBoca);
                }
                
                PatrolTarget = FindClosestBoca().transform.position;

                NavMeshAgent.SetDestination(PatrolTarget);
                if (NavMeshAgent.velocity == Vector3.zero)
                {
                    NavMeshAgent.Resume();
                }
            }

            yield return new WaitForSeconds(1.0f);
        }
    }

    private IEnumerator AI_HoldBoca()
    {
        while (true)
        {
            if (EnemySeen(out ChaseTarget))
            {
                ChangeState(AvaliableStates.Chase);
            }
            else
            {
                NavMeshAgent.SetDestination(_currentBoca.transform.position);
            }

            yield return new WaitForSeconds(1.0f);
        }
    }

    protected bool BocaInSight(out GameObject boca)
    {
        GameObject closestBoca = null;
        foreach (var col in Physics.OverlapSphere(transform.position, SightRadius, BocaLayerMask))
        {
            if (closestBoca == null)
            {
                closestBoca = col.gameObject;
            }
            else
            {
                var distanceClosest = Vector3.Distance(transform.position, closestBoca.gameObject.transform.position);
                var distanceThis = Vector3.Distance(transform.position, col.gameObject.transform.position);
                if (distanceThis <= distanceClosest)
                {
                    closestBoca = col.gameObject;
                }
            }
        }
        boca = closestBoca;
        return boca != null;
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CombatControl))]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyBehavior : MonoBehaviour {

    public enum AvaliableStates
    {
        Patrol,Chase,Attack,HoldingBoca
    }
    #region Chached Variables

    public AvaliableStates CurrentState;
    protected NavMeshAgent NavMeshAgent;
    protected CombatControl CombatControl;

    #endregion

    #region Patrol State Variables

    public List<GameObject> Bocas;

    protected Vector3 PatrolTarget;

    #endregion

    #region Chase State Variables

    public float SightRadius = 5;

    protected GameObject ChaseTarget;

    #endregion

    #region Attack State Variables

    protected float AttackRadius;

    protected GameObject AttackTarget;


    #endregion

    protected void Initialize()
    {
        CurrentState = AvaliableStates.Patrol;
        NavMeshAgent = GetComponent<NavMeshAgent>();
        CombatControl = GetComponent<CombatControl>();
        CombatControl.enabled = false;
        AttackRadius = CombatControl.AttackRadius;
        foreach (Transform boca in GameObject.Find("Pivot Bocas").transform)
        {
            Bocas.Add(boca.gameObject);
        }
    }
    void Awake()
    {
        Initialize();
    }

    void Start()
    {
        ChangeState(CurrentState);
    }

    void OnTriggerEnter(Collider other)
    {
        if (Bocas.Contains(other.gameObject))
        {
            Bocas.Remove(other.gameObject);
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, SightRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackRadius);

    }

    protected virtual void ChangeState(AvaliableStates active)
    {
        StopAllCoroutines();

        CurrentState = active;

        switch (CurrentState)
        {
            case AvaliableStates.Attack:
                StartCoroutine(AI_Attack());
                return;
            case AvaliableStates.Chase:
                StartCoroutine(AI_Chase());
                return;
            case AvaliableStates.Patrol:
                StartCoroutine(AI_Patrol());
                return;
        }
    }

    protected virtual IEnumerator AI_Attack()
    {
        while (true)
        {
            if (!EnemyInRange(out AttackTarget))
            {
                CombatControl.enabled = false;
                ChangeState(AvaliableStates.Chase);
            }
            else
            {
                NavMeshAgent.Stop();
                CombatControl.enabled = true;
            }

            yield return new WaitForFixedUpdate();
        }
    }

    protected virtual IEnumerator AI_Chase()
    {
        while (true)
        {
            if (!EnemySeen(out ChaseTarget))
            {
                ChangeState(AvaliableStates.Patrol);
            }
            else
            {
                if (EnemyInRange(out AttackTarget))
                {
                    ChangeState(AvaliableStates.Attack);
                }
                else
                {
                    NavMeshAgent.SetDestination(ChaseTarget.transform.position);
                    if (NavMeshAgent.velocity == Vector3.zero)
                    {
                        NavMeshAgent.Resume();
                    }
                }
            }

            yield return new WaitForSeconds(1.0f);
        }

    }

    protected virtual IEnumerator AI_Patrol()
    {
        while (true)
        {
            if (EnemySeen(out ChaseTarget))
            {
                ChangeState(AvaliableStates.Chase);
            }
            else
            {
                //PatrolTarget = FindClosestBocaWithNavmesh().transform.position;
                PatrolTarget = FindClosestBoca().transform.position;

                NavMeshAgent.SetDestination(PatrolTarget);
                if (NavMeshAgent.velocity == Vector3.zero)
                {
                    NavMeshAgent.Resume();
                }
            }

            yield return new WaitForSeconds(1.0f);
        }
    }


    protected GameObject FindClosestBocaWithNavmesh()
    {
        var closestDistance = Mathf.Infinity;
        GameObject closestBoca = null;
        foreach (var boca in Bocas)
        {
            NavMeshHit hit;
            if (!NavMesh.SamplePosition(boca.transform.position, out hit, Mathf.Infinity, NavMesh.AllAreas)) continue;
            if (!(hit.distance < closestDistance)) continue;
            closestDistance = hit.distance;
            closestBoca = boca;
        }
        return closestBoca;
    }
    protected GameObject FindClosestBoca()
    {
        GameObject closestBoca = null;

        foreach (var boca in Bocas)
        {
            if (closestBoca == null)
            {
                closestBoca = boca.gameObject;
            }
            else
            {
                var distanceClosest = Vector3.Distance(transform.position, closestBoca.gameObject.transform.position);
                var distanceThis = Vector3.Distance(transform.position, boca.gameObject.transform.position);

                if (distanceThis <= distanceClosest)
                {
                    closestBoca = boca.gameObject;
                }
            }
        }
        return closestBoca;
    }

    protected bool EnemySeen(out GameObject enemy)
    {
        enemy = FindClosestGameObjectInsideRadius(SightRadius);

        return enemy != null;
    }

    protected bool EnemyInRange(out GameObject enemy)
    {
        enemy = FindClosestGameObjectInsideRadius(AttackRadius);

        return enemy != null;
    }

    protected GameObject FindClosestGameObjectInsideRadius(float radius)
    {
        GameObject closestEnemy = null;
        foreach (var col in Physics.OverlapSphere(transform.position, radius, CombatControl.EnemyLayers))
        {
            if (closestEnemy == null)
            {
                closestEnemy = col.gameObject;
            }
            else
            {
                var distanceClosest = Vector3.Distance(transform.position, closestEnemy.gameObject.transform.position);
                var distanceThis = Vector3.Distance(transform.position, col.gameObject.transform.position);

                if (distanceThis <= distanceClosest)
                {
                    closestEnemy = col.gameObject;
                }
            }
        }
        return closestEnemy;
    }

}

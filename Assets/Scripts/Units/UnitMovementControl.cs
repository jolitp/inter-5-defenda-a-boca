﻿using UnityEngine;

[RequireComponent(typeof(UnitSelectionControl))]
[RequireComponent(typeof(NavMeshAgent))]
public class UnitMovementControl : MonoBehaviour
{
    public GameObject DestinationIndicator;
    private UnitSelectionControl _unitSelectionControl;
    private NavMeshAgent _navMeshAgent;

	// Use this for initialization
	void Start ()
	{
	    _unitSelectionControl = GetComponent<UnitSelectionControl>();
	    _navMeshAgent = GetComponent<NavMeshAgent>();
;	}
	
	// Update is called once per frame
	void Update () {
	    if (_unitSelectionControl.selected && Input.GetMouseButtonUp(1))
	    {
	        var destination = CameraOperator.GetDestination();

	        if (destination != Vector3.zero)
            {
	            GameObject.Instantiate(DestinationIndicator, destination, Quaternion.identity);
	            _navMeshAgent.SetDestination(destination);
	        }
	    }
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IaPolícia : MonoBehaviour {

	public int nAlvos = 0;
	float mDistancia = 5000;

	GameObject alvoFinal;

	public List<GameObject> pos;
	private GameObject Final;

	private NavMeshAgent agent;

	// Use this for initialization
	void Start ()
    {
		pos = new List<GameObject>();

		agent = GetComponent<NavMeshAgent>();

		Final = GameObject.Find("Final");

		for (int i = 0; i <= pos.Count; i++)
        {
			if (GameObject.Find("Alvo " + i) != null)
			pos.Add(GameObject.Find("Alvo " + i)); // faz uma list de todos os alvos, sendo alvos as bocas, menos a boca central
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (alvoFinal == null)
        {
			closer();
			goTo(alvoFinal);
		}

		if (alvoFinal.GetComponent<Alvo>().active == false && alvoFinal != null){
			alvoFinal = null;
		}

	}

	void closer(){
        float dist = 0;
		for (int i = 1; i <= pos.Count - 1; i++){
			dist = Vector3.Distance(gameObject.transform.position, pos[i].transform.position); // mede a distancia entre a unidade e todos os alvos
			
			if (pos[i].GetComponent<Alvo>().active){ //Descobre se o Alvo contem ao menos um vapor
				if (dist < mDistancia){
					mDistancia = dist;
					alvoFinal = pos[i]; //garante que o alvo selecionado e o mais perto
					Debug.Log(alvoFinal.name);
				}
			}
		}
	}

	void goTo(GameObject target){

		if (target != null){
			agent.SetDestination(target.transform.position); // anda ate o alvo
		}
		else{
			agent.SetDestination(Final.transform.position);
		}
	}

}











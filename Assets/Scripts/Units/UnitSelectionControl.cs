﻿using UnityEngine;
using UnityEngine.UI;

public class UnitSelectionControl : MonoBehaviour
{
    public bool selected = false;
    //public float flootOffset = 0.5f;

    private Renderer _renderer;
    private Color _originalColor;
    private Image _reticle;

    void Start()
    {
        _renderer = GetComponentInChildren<Renderer>();
        _reticle = GetComponentInChildren<Image>();
    }

	void Update () {
        if (_renderer.isVisible && Input.GetMouseButton(0))
	    {
	        var camPos = Camera.main.WorldToScreenPoint(transform.position);
	        camPos.y = CameraOperator.InvertMouseY(camPos.y);
	        selected = CameraOperator.Selection.Contains(camPos);

	        if (selected)
	        {
	            _reticle.enabled = true;
	        }
	        else
	        {
	            _reticle.enabled = false;
	        }
	    }
	}
}

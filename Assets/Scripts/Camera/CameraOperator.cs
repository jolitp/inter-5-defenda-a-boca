﻿using UnityEngine;
using System.Collections.Generic;
public class CameraOperator : MonoBehaviour
{
    public Texture2D SelectionHighlight;
    public static Rect Selection = new Rect(0, 0, 0, 0);
    public GameObject CameraGuide;

    private Vector3 _startClick = -Vector3.one;
    private static Vector3 _moveToDestination = Vector3.zero;
    private static List<string> passables = new List<string>() { "Floor" }; 

	void Update ()
	{
	    CheckCamera();
	    CleanUp();
	}

    void OnGUI()
    {
        if (_startClick != -Vector3.one)
        {
            GUI.color = new Color(1, 1, 1, 0.5f);
            GUI.DrawTexture(Selection, SelectionHighlight);
        }
    }

    private void CheckCamera()
    {
        if(Input.GetMouseButtonDown(0))
        {
            _startClick = Input.mousePosition;
        } else if(Input.GetMouseButtonUp(0))
        {
            _startClick = -Vector3.one;
        }
        if (Input.GetMouseButton(0))
        {
            Selection = new Rect(_startClick.x,
                InvertMouseY(_startClick.y),
                Input.mousePosition.x - _startClick.x,
                InvertMouseY( Input.mousePosition.y) - InvertMouseY(_startClick.y)
                );
        }

        if (Selection.width < 0)
        {
            Selection.x += Selection.width;
            Selection.width = -Selection.width;
        }
        if (Selection.height < 0)
        {
            Selection.y += Selection.height;
            Selection.height = -Selection.height;
        }
    }

    public static Vector3 GetDestination()
    {
        if (_moveToDestination == Vector3.zero)
        {
            RaycastHit hit;
            var r = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(r, out hit))
            {
                while (!passables.Contains(hit.transform.name))
                {
                    if (!Physics.Raycast(hit.point + r.direction * 0.1f, r.direction, out hit))
                    {
                        break;
                    }
                }
            }
            if (hit.transform != null)
            {
                _moveToDestination = hit.point;
            }
        }

        return _moveToDestination;
    }
    public static Vector3 GetDestination(Camera targetCam)
    {
        if (_moveToDestination == Vector3.zero)
        {
            RaycastHit hit;
            var r = targetCam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(r, out hit))
            {
                while (!passables.Contains(hit.transform.name))
                {
                    if (!Physics.Raycast(hit.point + r.direction * 0.1f, r.direction, out hit))
                    {
                        break;
                    }
                }
            }
            if (hit.transform != null)
            {
                _moveToDestination = hit.point;
            }
        }

        return _moveToDestination;
    }

    private void CleanUp()
    {
        if (!Input.GetMouseButtonUp(1))
        {
            _moveToDestination = Vector3.zero;
        }
    }

    public static float InvertMouseY(float y)
    {
        return Screen.height - y;
    }
}

﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{
    public Collider ArenaBoundaries;
    public Camera MinimapCamera;

    public float MinimumSpeed = 1;
    public float MaximumSpeed = 1;

    void Start()
    {
        MinimapCamera = GameObject.Find("Minimap Camera").GetComponent<Camera>();
        ArenaBoundaries = GameObject.Find("Arena Boundairies").GetComponent<Collider>();
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0) && MinimapCamera.pixelRect.Contains(Input.mousePosition))
        {
            var destination = CameraOperator.GetDestination(MinimapCamera);

            if (destination != Vector3.zero)
            {
                transform.position = destination;
            }
        }
    }

    public void StartForward()
    {
        StartCoroutine(MoveForward());
    }

    public void StartBackward()
    {
        StartCoroutine(MoveBackward());
    }

    public void StartLeft()
    {
        StartCoroutine(MoveLeft());
    }

    public void StartRight()
    {
        StartCoroutine(MoveRight());
    }

    public void StartForwardLeft()
    {
        StartCoroutine(MoveForward());
        StartCoroutine(MoveLeft());
    }

    public void StartForwardRight()
    {
        StartCoroutine(MoveForward());
        StartCoroutine(MoveRight());
    }

    public void StartBackwardLeft()
    {
        StartCoroutine(MoveBackward());
        StartCoroutine(MoveLeft());
    }

    public void StartBackwardRight()
    {
        StartCoroutine(MoveBackward());
        StartCoroutine(MoveRight());
    }

    private bool IsInsideArenaBounds()
    {
        return ArenaBoundaries.bounds.Contains(gameObject.transform.position);
    }


    private IEnumerator MoveForward()
    {
        var moveSpeed = MinimumSpeed;
        do
        {
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
            if (moveSpeed <= MaximumSpeed)
            {
                moveSpeed += Time.deltaTime;
            }
            yield return 0;
        }
        while (IsInsideArenaBounds()) ;

    }
    private IEnumerator MoveBackward()
    {
        var moveSpeed = MinimumSpeed;
        do
        {
            transform.Translate(-Vector3.forward * moveSpeed * Time.deltaTime);
            if (moveSpeed <= MaximumSpeed)
            {
                moveSpeed += Time.deltaTime;
            }
            yield return 0;
        }
        while (IsInsideArenaBounds()) ;
    }
    private IEnumerator MoveLeft()
    {
        var moveSpeed = MinimumSpeed;
        do
        {
            transform.Translate(-Vector3.right * moveSpeed * Time.deltaTime);
            if (moveSpeed <= MaximumSpeed)
            {
                moveSpeed += Time.deltaTime;
            }
            //Debug.Log("speed " + moveSpeed + " position " + transform.position);
            yield return 0;
        }
        while (IsInsideArenaBounds()) ;
    }
    private IEnumerator MoveRight()
    {
        var moveSpeed = MinimumSpeed;
        do
        {
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
            if (moveSpeed <= MaximumSpeed)
            {
                moveSpeed += Time.deltaTime;
            }
            //Debug.Log("speed " + moveSpeed + " position " + transform.position);
            yield return 0;
        }
        while (IsInsideArenaBounds()) ;
    }
}

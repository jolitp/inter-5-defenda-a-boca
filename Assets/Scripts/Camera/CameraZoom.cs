﻿using UnityEngine;
using System.Collections;

public class CameraZoom : MonoBehaviour
{
    public int MinimumZoom;
    public int MaximumZoom;
    public int StepToZoom;

    private Camera _camera;
    
	void Start ()
	{
	    _camera = GetComponent<Camera>();
	}
	
	void Update () {
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (_camera.fieldOfView < MaximumZoom)
            {
                _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, MaximumZoom, StepToZoom * Time.deltaTime);
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (_camera.fieldOfView > MinimumZoom)
            {
                _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, MinimumZoom, StepToZoom * Time.deltaTime);
            }
        }
	}
}

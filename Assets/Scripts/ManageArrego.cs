﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageArrego : MonoBehaviour
{
    public GameObject ArregoPanel;

    private Text _text;
    private int _arregoValue;
    private int _spawnPointIndex;
    private int _quantityToSpawn;
    private EnemyUnitSpawner _enemyUnitSpawner;

	// Use this for initialization
	void Start ()
	{
	    _enemyUnitSpawner = GameObject.Find("Pivot SpawnPoints").GetComponent<EnemyUnitSpawner>();
	    _text = ArregoPanel.transform.FindChild("Text").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnEnable()
    {
        Messenger<int,int,int>.AddListener("arrego", DisplayArregoMessege);
    }

    void OnDisable()
    {
        Messenger<int,int,int>.RemoveListener("arrego", DisplayArregoMessege);
    }

    void DisplayArregoMessege(int arregoValue, int quantity, int spawnPointIndex)
    {
        ArregoPanel.SetActive(true);
        _arregoValue = arregoValue;
        _text.text = "Arrego " + arregoValue + " R$";
        _quantityToSpawn = quantity;
        _spawnPointIndex = spawnPointIndex;
        Messenger.Broadcast("game paused");
    }

    public void AcceptArrego()
    {
        Messenger<int>.Broadcast<bool>("remove money", _arregoValue, MoneyRemovedSucced);
    }

    void MoneyRemovedSucced(bool succeed)
    {
        if (succeed)
        {
            ArregoPanel.transform.Find("Aceitar").GetComponent<Button>().interactable = true;
            ArregoPanel.SetActive(false);
            Messenger.Broadcast("game unpaused");
            _arregoValue = 0;
        }
        else
        {
            ArregoPanel.transform.Find("Text").GetComponent<Text>().text = "Dinheiro Insuficiente";
            ArregoPanel.transform.Find("Aceitar").GetComponent<Button>().interactable = false;

        }
    }

    public void DeclineArrego()
    {
        ArregoPanel.SetActive(false);
        _enemyUnitSpawner.StartCoroutine(_enemyUnitSpawner.SpawnPolice(_quantityToSpawn, _spawnPointIndex));
        Messenger.Broadcast("game unpaused");
    }
}

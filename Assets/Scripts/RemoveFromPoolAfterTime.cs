﻿using UnityEngine;
using System.Collections;

public class RemoveFromPoolAfterTime : MonoBehaviour
{

    public float TimeToRemove = 2.0f;

    private Timer _timer;

	// Use this for initialization
	void Start () {
	    _timer = new Timer(TimeToRemove,true,RemoveGameObject);
	    StartCoroutine(_timer.Start());
	}

    void RemoveGameObject()
    {
        ObjectPool.Recycle(gameObject);
    }
}

﻿using UnityEngine;
using System.Collections;

public class CharAudioControl : MonoBehaviour {

	AudioSource botas;

	NavMeshAgent mov;

	// Use this for initialization
	void Start () {
		mov = gameObject.GetComponent<NavMeshAgent>();
		botas = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		isWalking();
	}

	void isWalking(){
		if ((mov.velocity.x != 0 || mov.velocity.z != 0) && botas.isPlaying == false){
			botas.volume = Random.Range(0.8f, 1);
			botas.pitch = Random.Range(0.8f, 1.1f);
			botas.Play();

		}
	}


}
